﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLP;
using System.IO;

public class NetworkIOManager : MonoBehaviour {

	private static NetworkIOManager m_Instance;
	public static NetworkIOManager Instance{
		get{return m_Instance;}
	}
	private void Awake() {
		if(m_Instance == null) m_Instance = this;
	}
	private static string m_NetWork = string.Empty;
	public static string Network {
		get{return m_NetWork;}
		set {m_NetWork = value;}
	}
	private static string m_Input = string.Empty;

	public static string Input{
		get{return m_Input;}
		set{m_Input = value;}
	}
	public void importData() {
		if(!Helpless.choiceFile(Main.dataType, out m_Input)) {
			Debug.LogError("Get files Data failed");
		}
	}

	public void exportData() {
		DateTime todayDate = DateTime.Now;
		string name  = "Data_" + todayDate.Month + "_" + todayDate.Day + "_" + todayDate.Hour + "_" + todayDate.Minute+ "_" + todayDate.Second;  
		Helpless.saveFile("Resources/_Data/" + name + Main.dataType, m_Input);
	}
	public void importNetwork() {
		Debug.Log("Click Import Network!");
		if(!Helpless.choiceFile(Main.networkType, out m_NetWork)) {
			Debug.LogError("Get files Data failed");
		}
	}
	public void exportNetwork(MLP.Network network =null, string name = "") {
		DateTime todayDate = DateTime.Now;
		if(name == "") {
			name  = "Network_" + todayDate.Month + "_" + todayDate.Day + "_" + todayDate.Hour + "_" + todayDate.Minute+ "_" + todayDate.Second;
		}
		if(network == null) {
			network = Main.Network;
		}
		ExportHelper.ExportNetwork(network, "Assets/Resources/_Network/" + name + Main.networkType);	
		//Helpless.saveFile("Resources/_Network/" + name + Main.networkType, m_NetWork);
	}
	public List<string> getAllFilesData() {
		List<string> datas;
		if(!Helpless.getAllfiles("Resources/_Data", Main.dataType,out datas)) return null;
		return datas;
	}
}
