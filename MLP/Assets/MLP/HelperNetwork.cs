﻿using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MLP
{
    public static class ExportHelper
    {
        public static void ExportNetwork(Network network, string name = "")
        {
            var dn = GetHelperNetwork(network);
            if (!File.Exists(name))
            {
                var newFiles = File.Create(name);
                newFiles.Close();
            }
            
            using (var file = File.CreateText(name))
            {
                var serializer = new JsonSerializer { Formatting = Formatting.Indented };
                serializer.Serialize(file, dn);
            }
            return;
        }

        public static void ExportDatasets(List<DataSet> datasets)
        {
            return;
            //var dialog = new SaveFileDialog
            //{
            //    Title = "Save Dataset File",
            //    Filter = "Text File|*.txt;"
            //};

            //using (dialog)
            //{
            //    if (dialog.ShowDialog() != DialogResult.OK) return;
            //    using (var file = File.CreateText(dialog.FileName))
            //    {
            //        var serializer = new JsonSerializer { Formatting = Formatting.Indented };
            //        serializer.Serialize(file, datasets);
            //    }
            //}
        }

        private static HelperNetwork GetHelperNetwork(Network network)
        {
            var hn = new HelperNetwork
            {
                LearnRate = network.LearnRate,
                Momentum = network.Momentum
            };

            //Input Layer
            foreach (var n in network.InputLayer)
            {
                var neuron = new HelperNeuron
                {
                    Id = n.Id,
                    Bias = n.Bias,
                    BiasDelta = n.BiasDelta,
                    Gradient = n.Gradient,
                    Value = n.Value
                };

                hn.InputLayer.Add(neuron);

                foreach (var synapse in n.OutputSynapses)
                {
                    var syn = new HelperSynapse
                    {
                        Id = synapse.Id,
                        OutputNeuronId = synapse.OutputNeuron.Id,
                        InputNeuronId = synapse.InputNeuron.Id,
                        Weight = synapse.Weight,
                        WeightDelta = synapse.WeightDelta
                    };

                    hn.Synapses.Add(syn);
                }
            }

            //Hidden Layer
            foreach (var l in network.HiddenLayers)
            {
                var layer = new List<HelperNeuron>();

                foreach (var n in l)
                {
                    var neuron = new HelperNeuron
                    {
                        Id = n.Id,
                        Bias = n.Bias,
                        BiasDelta = n.BiasDelta,
                        Gradient = n.Gradient,
                        Value = n.Value
                    };

                    layer.Add(neuron);

                    foreach (var synapse in n.OutputSynapses)
                    {
                        var syn = new HelperSynapse
                        {
                            Id = synapse.Id,
                            OutputNeuronId = synapse.OutputNeuron.Id,
                            InputNeuronId = synapse.InputNeuron.Id,
                            Weight = synapse.Weight,
                            WeightDelta = synapse.WeightDelta
                        };

                        hn.Synapses.Add(syn);
                    }
                }

                hn.HiddenLayers.Add(layer);
            }

            //Output Layer
            foreach (var n in network.OutputLayer)
            {
                var neuron = new HelperNeuron
                {
                    Id = n.Id,
                    Bias = n.Bias,
                    BiasDelta = n.BiasDelta,
                    Gradient = n.Gradient,
                    Value = n.Value
                };

                hn.OutputLayer.Add(neuron);

                foreach (var synapse in n.OutputSynapses)
                {
                    var syn = new HelperSynapse
                    {
                        Id = synapse.Id,
                        OutputNeuronId = synapse.OutputNeuron.Id,
                        InputNeuronId = synapse.InputNeuron.Id,
                        Weight = synapse.Weight,
                        WeightDelta = synapse.WeightDelta
                    };

                    hn.Synapses.Add(syn);
                }
            }

            return hn;
        }
    }
    public class HelperNetwork
    {
        public double LearnRate { get; set; }
        public double Momentum { get; set; }
        public List<HelperNeuron> InputLayer { get; set; }
        public List<List<HelperNeuron>> HiddenLayers { get; set; }
        public List<HelperNeuron> OutputLayer { get; set; }
        public List<HelperSynapse> Synapses { get; set; }

        public HelperNetwork()
        {
            InputLayer = new List<HelperNeuron>();
            HiddenLayers = new List<List<HelperNeuron>>();
            OutputLayer = new List<HelperNeuron>();
            Synapses = new List<HelperSynapse>();
        }
    }

    public class HelperNeuron
    {
        public System.Guid Id { get; set; }
        public double Bias { get; set; }
        public double BiasDelta { get; set; }
        public double Gradient { get; set; }
        public double Value { get; set; }
    }

    public class HelperSynapse
    {
        public System.Guid Id { get; set; }
        public System.Guid OutputNeuronId { get; set; }
        public System.Guid InputNeuronId { get; set; }
        public double Weight { get; set; }
        public double WeightDelta { get; set; }
    }
}