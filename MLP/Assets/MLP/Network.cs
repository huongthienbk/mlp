﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
namespace MLP
{
    public class Network
    {
        #region -- Properties --
        public double LearnRate { get; set; }
        public double Momentum { get; set; }
        public List<Neuron> InputLayer { get; set; }
        public List<List<Neuron>> HiddenLayers { get; set; }
        public List<Neuron> OutputLayer { get; set; }
        #endregion

        #region -- Globals --
        private static readonly System.Random Random = new System.Random();
        #endregion

        #region -- Constructor --
        public Network()
        {
            LearnRate = 0;
            Momentum = 0;
            InputLayer = new List<Neuron>();
            HiddenLayers = new List<List<Neuron>>();
            OutputLayer = new List<Neuron>();
        }

        public Network(int inputSize, int[] hiddenSizes, int outputSize, double? learnRate = null, double? momentum = null)
        {
            LearnRate = learnRate ?? .4;
            Momentum = momentum ?? .9;
            InputLayer = new List<Neuron>();
            HiddenLayers = new List<List<Neuron>>();
            OutputLayer = new List<Neuron>();

            for (var i = 0; i < inputSize; i++)
                InputLayer.Add(new Neuron(i));

            var firstHiddenLayer = new List<Neuron>();
            for (var i = 0; i < hiddenSizes[0]; i++)
                firstHiddenLayer.Add(new Neuron(InputLayer, i));

            HiddenLayers.Add(firstHiddenLayer);

            for (var i = 1; i < hiddenSizes.Length; i++)
            {
                var hiddenLayer = new List<Neuron>();
                for (var j = 0; j < hiddenSizes[i]; j++)
                    hiddenLayer.Add(new Neuron(HiddenLayers[i - 1], j));
                HiddenLayers.Add(hiddenLayer);
            }

            for (var i = 0; i < outputSize; i++)
                OutputLayer.Add(new Neuron(HiddenLayers.Last(), i));
        }
        #endregion

        #region -- Training --
        public void Train(List<DataSet> dataSets, int numEpochs)
        {
            for (var i = 0; i < numEpochs; i++)
            {
                foreach (var dataSet in dataSets)
                {
                    ForwardPropagate(dataSet.Values);
                    BackPropagate(dataSet.Targets);
                }
            }
        }

        public IEnumerator Train(List<DataSet> dataSets, double minimumError)
        {
            var error = 1.0;
            var numEpochs = 0;

            while (error > minimumError && numEpochs < int.MaxValue)
            {
                var errors = new List<double>();
                foreach (var dataSet in dataSets)
                {
                    
                    Main.Instance.setColorInput(dataSet);
                    Main.Instance.setColorTarget(dataSet);
                    // Debug.LogWarning(dataSet.Targets[0]);
                    ForwardPropagate(dataSet.Values);
                    BackPropagate(dataSet.Targets);
                    errors.Add(CalculateError(dataSet.Targets));
                    yield return new WaitForSeconds(0.03f);
                }
                if((numEpochs + 1) % 10000 == 0) {
                        NetworkIOManager.Instance.exportNetwork(this, "Network_" + numEpochs);
                        Debug.LogWarning("Network export " + numEpochs);
                } 
                error = errors.Average();
                Debug.Log("Error = " + error);
                numEpochs++;
                
            }
            yield return new WaitForFixedUpdate();
        }

        private void ForwardPropagate(params double[] inputs)
        {
            var i = 0;
            double result = 0.0;
            InputLayer.ForEach(a => a.Value = inputs[i++]);
            //Debug.Log(HiddenLayers.Count +  " 0 = " + HiddenLayers[0].Count+  " 1 = " + HiddenLayers[1].Count);
            for (i = 0; i < HiddenLayers.Count; i++)
            {
                for (int j = 0; j < HiddenLayers[i].Count; j++)
                {
                    result = HiddenLayers[i][j].CalculateValue();
                    //Main.Instance.setColorHidden(i, j, result);
                }
            }
            // HiddenLayers.ForEach(a => a.ForEach(b => b.CalculateValue()));
            for (i = 0; i < OutputLayer.Count; i++)
            {
                 result = OutputLayer[i].CalculateValue();
                 Main.Instance.setColorOutput(i, result);
            }
        }

        private void BackPropagate(params double[] targets)
        {
            var i = 0;
            OutputLayer.ForEach(a => a.CalculateGradient(targets[i++]));
            HiddenLayers.Reverse();
            HiddenLayers.ForEach(a => a.ForEach(b => b.CalculateGradient()));
            HiddenLayers.ForEach(a => a.ForEach(b => b.UpdateWeights(LearnRate, Momentum)));
            HiddenLayers.Reverse();
            OutputLayer.ForEach(a => a.UpdateWeights(LearnRate, Momentum));
        }

        public double[] Compute(params double[] inputs)
        {
            ForwardPropagate(inputs);
            return OutputLayer.Select(a => a.Value).ToArray();
        }

        private double CalculateError(params double[] targets)
        {
            var i = 0;
            return OutputLayer.Sum(a => Math.Abs(a.CalculateError(targets[i++])));
        }
        #endregion

        #region -- Helpers --
        public static double GetRandom()
        {
            return 2 * Random.NextDouble() - 1;
        }
        #endregion
    }

    #region -- Enum --
    public enum TrainingType
    {
        Epoch,
        MinimumError
    }
    #endregion
}