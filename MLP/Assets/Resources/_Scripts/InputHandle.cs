﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputHandle : MonoBehaviour {

	[SerializeField] UnityEvent m_MouseDown;


	private void OnMouseDown() {
		if(m_MouseDown != null)  {
			m_MouseDown.Invoke();
		}
	}
}
