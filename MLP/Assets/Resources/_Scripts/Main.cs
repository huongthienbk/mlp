﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
namespace MLP
{
	public class Main : MonoBehaviour {

		// Use this for initialization
		#region -- Variables --
		private static int _numInputParameters = 2;
		private static int _numHiddenLayers = 2;
		private static int[] _hiddenNeurons = new int[2] { 1000, 10 };
		private static int _numOutputParameters = 1;
		private static Network _network;
		public static Network Network{get{return _network;}}
		//private static List<DataSet> _dataSets;
		#endregion
		#region UI Input
		public TextAsset m_NetWorkFile;
		public TextAsset m_InputFile;
		public List<GameObject> inputNodes;
		private List<Image> m_InputNodes = new List<Image>();

		public GameObject targetNodes;
		private List<Image> m_TargetNodes = new List<Image>();
		public List<GameObject> hiddenNode;
		private List<Image> m_HiddenNodes1= new List<Image>();
		private List<Image> m_HiddenNodes2= new List<Image>();
		public GameObject outputNode;
		private List<Image> m_OutputNodes= new List<Image>();
		private string m_NetWorkContent ="";
		private Network m_Network;
		private string m_Input = "";
		private List<DataSet> data = new List<DataSet>();
		private static Main _instance;
		public enum STATUS {
			RUNNING,
			BEGIN,
			END,
			COUNT,
		}
		public static string dataType = "xottdata";
		public static string networkType = "xonetwork";
		public STATUS m_Status = STATUS.BEGIN;
		public static Main Instance {
			get {return _instance;}
		}
		#endregion
		private void Awake() {
			if(_instance == null) _instance = this;
		}
		void Start () {
			// Debug.Log(data.Count);
			init();
			//run();			
		}
		public void testEvent() {
			Debug.LogError("Event run!");
		}
		public void run() {
			if(m_Status != STATUS.BEGIN) return;
			// string data = string.Empty;
			
			
			if(NetworkIOManager.Input == string.Empty) {
				List<string> datas;
				if(!Helpless.getAllfiles("Resources/_Data", Main.dataType,out datas)) {
					if(!Helpless.choiceFile(dataType, out m_Input)) {
						Debug.LogError("Get data failed!");
						return;
					}
					NetworkIOManager.Input = m_Input;
					data = JsonConvert.DeserializeObject<List<DataSet>>(m_Input);
				}
				else {
					foreach (var item in datas)
					{
						var dataTemp =  JsonConvert.DeserializeObject<List<DataSet>>(item);
						NetworkIOManager.Input += item;
						data.AddRange(dataTemp);
					}
				}
			}
			else {
				Debug.LogWarning("Input: " + NetworkIOManager.Input);
				data = JsonConvert.DeserializeObject<List<DataSet>>(NetworkIOManager.Input);
			}
			if(NetworkIOManager.Network == string.Empty) {
				if(!Helpless.choiceFile(networkType, out m_NetWorkContent)) {
					Debug.LogError("Get network failed!");
					return;
				}
				NetworkIOManager.Network = m_NetWorkContent;
				m_Network =  ImportHelper.ImportNetwork(JsonConvert.DeserializeObject<HelperNetwork>(m_NetWorkContent));
			} 
			else {
				m_Network =  ImportHelper.ImportNetwork(JsonConvert.DeserializeObject<HelperNetwork>(NetworkIOManager.Network));
			}
			if(data.Count > 0) {
				m_Status = STATUS.RUNNING;
				Debug.LogError(data.Count);
				CreateHandleNN(data, m_Network);
			}
		}
		void init() {
			setupInputUI();
			setupHiddenUI();
			setupOutputUI ();
			setupTargetUI();
		}
		void setupInputUI() {
			foreach (var item in inputNodes)
			{
				var temp = item.GetComponentsInChildren<Image>();
				foreach (var node in temp)
				{
					if(node.name.Contains("input")) continue;	
					m_InputNodes.Add(node);
				}
			}
		}
		void setupTargetUI() {
			var temp = targetNodes.GetComponentsInChildren<Image>();
			foreach (var node in temp)
			{
				if(node.name.Contains("target")) continue;	
				m_TargetNodes.Add(node);
			}
		}
		void setupHiddenUI () {
			if(hiddenNode.Count != 3) return;
			Image[] temp;
			for (int i = 0; i < 2; i++)
			{
				temp = hiddenNode[i].GetComponentsInChildren<Image>();
				foreach (var node in temp)
				{
					if(node.name.Contains("hidden")) continue;	
					m_HiddenNodes1.Add(node);
				}
			}
			Debug.Log("Hidden count 1 = " + m_HiddenNodes1.Count);
			temp = hiddenNode[2].GetComponentsInChildren<Image>();
			foreach (var node in temp)
			{
				if(node.name.Contains("hidden")) continue;	
				m_HiddenNodes2.Add(node);
			}
			Debug.Log("Hidden count 2 = " + m_HiddenNodes2.Count);
		}
		void setupOutputUI () {
			Image[] temp;
			temp = outputNode.GetComponentsInChildren<Image>();
			foreach (var node in temp)
			{
				if(node.name.Contains("output")) continue;	
				m_OutputNodes.Add(node);
			}
			Debug.Log("Output = " + m_OutputNodes.Count);
		}
		private void CreateHandleNN(List<DataSet> _dataSets, Network _network)
        {
            //_network = new Network(160, new int[2] { 1000, 10 }, 52);
			// setColorInput(_dataSets[0]);
			// setColorTarget(_dataSets[0]);
			StartCoroutine(_network.Train(_dataSets, 0.01f));
            //DatasetMenu();
            //NetworkMenu();
            //NetworkMenu();
        }
		public void setColorInput(DataSet _dataSets) {
			for (int i = 0; i < _dataSets.Values.Length; i++)
			{
				if(_dataSets.Values[i] == 0) {
					m_InputNodes[i].color = Color.white;
				}
				else {
					m_InputNodes[i].color = Color.red;
				}
			}
		}
		public void setColorTarget(DataSet _dataSets) {
			for (int i = 0; i < _dataSets.Targets.Length; i++)
			{
				if(_dataSets.Targets[i] == 0) {
					m_TargetNodes[i].color = Color.white;
				}
				else {
					m_TargetNodes[i].color = Color.red;
				}
			}
		}
		private double threhold = 0.7f;
		public void setColorHidden(int layerNumber, int nodeNumber, double value = 0.0f) {
			switch (layerNumber)
			{
				case 0:
					if(value > threhold)
						m_HiddenNodes1[nodeNumber].color = Color.red;
					else 
						m_HiddenNodes1[nodeNumber].color = Color.white;
					break;
				case 1: 
					if(value > threhold)
						m_HiddenNodes2[nodeNumber].color = Color.red;
					else 
						m_HiddenNodes2[nodeNumber].color = Color.white;
					break;
				default:
					break;
			}
		}
		public void setColorOutput(int nodeNumber, double value = 0.0f) {
			if(value > threhold)
				m_OutputNodes[nodeNumber].color = Color.red;
			else 
				m_OutputNodes[nodeNumber].color = Color.white;
		}
	}
	
}
