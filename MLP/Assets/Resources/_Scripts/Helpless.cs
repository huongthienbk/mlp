﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class Helpless : EditorWindow
	{
		[MenuItem("Example/Overwrite Texture")]
		public static void Apply()
		{
			string m_String = null;
			// if (m_String == null)
			// {
			// 	EditorUtility.DisplayDialog("Select Texture", "You must select a texture first!", "OK");
			// 	return;
			// }

			string path = EditorUtility.OpenFilePanel("Overwrite with png", "", "*");
			if (path.Length != 0)
			{
				var fileContent = File.ReadAllText(path);
				Debug.LogError(fileContent);
				// texture.LoadImage(fileContent);
			}
		}
		public static bool choiceFile(string type, out string content)
		{
			content = string.Empty;
			string m_String = null;
			// if (m_String == null)
			// {
			// 	EditorUtility.DisplayDialog("Select Texture", "You must select a texture first!", "OK");
			// 	return;
			// }

			string path = EditorUtility.OpenFilePanel("Choice file " + type, "", type);
			Debug.Log(path);
			if(!checkType(path)) {
				return false;
			}
			if (path.Length != 0)
			{
				try
				{
					var fileContent = File.ReadAllText(path);
					content = fileContent;
					Debug.LogWarning("content " + content);
					Debug.LogWarning("Files Choice correct, lenght " + fileContent.Length);
					return true;
					// texture.LoadImage(fileContent);
				}
				catch (System.Exception)
				{
					Debug.LogWarning("Choice file faild");
					return false;
				}
				
			}
			return false;
		}

		public static bool getAllfiles(string _realPath, string _type, out List<string> _content)
		{
			_content = new List<string>();
			var paths = Directory.GetFiles(Application.dataPath +"/" + _realPath,"*" + _type);
			if(paths == null) return false;
			var contentTemp = string.Empty;
			foreach (var path in paths)
			{
				try
				{
					var fileContent = File.ReadAllText(path);
					Debug.LogWarning("content " + fileContent);
					_content.Add(fileContent);
					Debug.LogWarning("Files Choice correct, lenght" + fileContent.Length);
					// return true;
					// texture.LoadImage(fileContent);
				}
				catch (System.Exception)
				{
					Debug.LogWarning("Choice file faild");
					return false;
				}
			}
			if(_content.Count != 0) return true;
			return false;
		}
		static bool checkType(string path) {
			//hien thuc check o day!
			return true;
		}
		public static bool saveFile(string fileName, string content) {
			string path = Application.dataPath +"/" + fileName;
			Debug.LogError(path);
			Debug.LogError(content);
			File.WriteAllText(path, content);
			return true;
		}
	}