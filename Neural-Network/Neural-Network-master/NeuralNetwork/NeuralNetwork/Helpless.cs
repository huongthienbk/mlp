﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using NeuralNetwork.NetworkModels;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNetwork.Helpers;
using System.Linq;

namespace NeuralNetwork
{
    public class Helpless
    {
        public static string NETWORK_TYPE = "xonetwork";
        public static string DATA_TYPE = "xottdata";
        public static string networkName = string.Empty;
        public static int[][] HIDDEN_NODE = new int[0][];
        public static int numb_o_Files = 10;
        public static int[] s_current_Time = new int[2] { 0, 0 };
        public static int numb_o_Record = 10;
        public static List<List<int>> timeSnap = new List<List<int>>();
        public static bool shouldNewNetWork = false;
        public static bool saveNameFormatNetwork = false;
        public static string CONFIG_PATH = "..\\..\\Data\\_Configs\\time_export_network.txt";
        public static string TIME_POS = "..\\..\\Data\\_Configs\\time_pos.txt";
        public static string PATH_NETWORK_TRAIN = "..\\..\\Data\\_Networks";
        public static string PATH_NETWORK_TEMP = "..\\..\\Data\\_NetworkTemp";
        public static string PATH_DATA_TRAIN_AI = "..\\..\\Data\\_Datas\\DataTrain\\AI\\TruyenThong";
        public static string PATH_DATA_TRAIN_REAL = "..\\..\\Data\\_Datas\\DataTrain\\Real\\TruyenThong";
        public static string PATH_RESUME_NETWORK = "..\\..\\Data\\_NetworksSave\\";
        public static string NAME_RESUME_NETWORK = string.Empty;
        public static string PATH_DATA_MAIN_TRAIN = string.Empty;
        public static List<DataSet> getAlDatasets(string _type)
        {
            if (PATH_DATA_MAIN_TRAIN == string.Empty) PATH_DATA_MAIN_TRAIN = PATH_DATA_TRAIN_REAL;
            List<DataSet> _content = new List<DataSet>();
            var paths = Directory.GetFiles(PATH_DATA_MAIN_TRAIN, "*" + _type);
            if (paths == null) return null;
            Console.WriteLine("paths lenght: " + paths.Length);
            int dem = 0;
            foreach (var path in paths)
            {
                try
                {
                    dem++;
                    if (dem > numb_o_Files) break;
                    var fileContent = File.ReadAllText(path);
                    var datasBatch = JsonConvert.DeserializeObject<List<DataSet>>(fileContent);
                    _content.AddRange(datasBatch);
                    //Debug.LogWarning("Files Choice correct, lenght" + fileContent.Length);
                    // return true;
                    // texture.LoadImage(fileContent);
                }
                catch (System.Exception)
                {
                    //Debug.LogWarning("Choice file faild");
                    return null;
                }
            }
            if (_content.Count != 0) { return _content; };
            return null;
        }
        public static Network getMap(string _type)
        {
            List<DataSet> _NetWorkTemp = new List<DataSet>();
            var pathNetwork = Directory.GetFiles(PATH_NETWORK_TRAIN, "*" + _type).OrderByDescending(d => new FileInfo(d).CreationTime);
            string[] pathsNetworkTemp = Directory.GetFiles(PATH_NETWORK_TEMP, "*" + _type);

            if (pathNetwork == null || pathNetwork.Count() == 0) { return null; }
            if (pathsNetworkTemp != null)
            {
                if (pathsNetworkTemp.Count() > 0)
                {
                    foreach (var path in pathsNetworkTemp)
                    {
                        try
                        {
                            File.Delete(path);
                        }
                        catch (System.Exception)
                        {
                            //Debug.LogWarning("Choice file faild");
                            return null;
                        }
                    }
                }
            }
            foreach (var item in pathNetwork)
            {
                Console.WriteLine(item);
            }
            //Console.ReadLine();
            Console.WriteLine("Files get Network name = " + pathNetwork.First());
            string fileContent = "";
            Network mainNetwork = null;
            string content = string.Empty;
            bool getNetworkDone = false;
            do
            {
                pathNetwork = Directory.GetFiles(PATH_NETWORK_TRAIN, "*" + _type).OrderByDescending(d => new FileInfo(d).CreationTime);
                using (var file = File.OpenText(pathNetwork.First()))
                {
                    content = file.ReadToEnd();
                    if (content != "") {
                        HelperNetwork helperNetwork = JsonConvert.DeserializeObject<HelperNetwork>(content);
                        mainNetwork = ImportHelper.ImportNetwork(helperNetwork);
                        getNetworkDone = true;
                        break;
                    }
                    else
                    {
                        File.Delete(pathNetwork.First());
                    }

                }

            } while (!getNetworkDone);
            //mainNetwork = JsonConvert.DeserializeObject<Network>(fileContent);
            if (mainNetwork != null)
                ExportHelper.ExportNetwork(mainNetwork, "NetworkTemp", "_NetworkTemp");
            foreach (var path in pathNetwork)
            {
                try
                {
                    File.Delete(path);
                }
                catch (System.Exception)
                {
                    //Debug.LogWarning("Choice file faild");
                    return null;
                }
            }
            return mainNetwork;
        }
        public static Network getMap()
        {
            Network mainNetwork = null;
            string content = string.Empty;
            if(NAME_RESUME_NETWORK != "0")
            {
                if(File.Exists(PATH_RESUME_NETWORK + NAME_RESUME_NETWORK))
                {
                    using (var file = File.OpenText(PATH_RESUME_NETWORK + NAME_RESUME_NETWORK))
                    {
                        if (file == null) return null;
                        content = file.ReadToEnd();
                        if (content != "")
                        {
                            HelperNetwork helperNetwork = JsonConvert.DeserializeObject<HelperNetwork>(content);
                            mainNetwork = ImportHelper.ImportNetwork(helperNetwork);
                            return mainNetwork;
                        }

                    }
                }
            }
            
            return null;
        } 
        public static bool updateConfigTimePos(string content)
        {
            try
            {
                File.WriteAllText(TIME_POS, content);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool getConfig()
        {
            #region time export network
            int i; 
            using (var configContent = File.OpenText(CONFIG_PATH)) {
                if(configContent != null)
                {
                    string line = string.Empty;
                    int dem = 0;
                    string[] record;
                    while((line = configContent.ReadLine()) != null)
                    {
                        dem++;
                        if(dem == 1) //number of files have to get
                        {
                            numb_o_Files = int.Parse(line);
                            Console.WriteLine("Number of files = " + numb_o_Files);
                            continue;
                        }
                        record = line.Split(' ');
                        if(dem == 2)
                        {
                            numb_o_Record = int.Parse(line);
                            HIDDEN_NODE = new int[numb_o_Record][];
                            Console.WriteLine("Number of Record = " + numb_o_Record);
                            continue;
                        }
                        if(record.Length > 2)
                        {
                            switch (record[0])
                            {
                                case "NC":
                                    Console.WriteLine("NC mode");
                                    PATH_DATA_MAIN_TRAIN = PATH_DATA_TRAIN_REAL;
                                    break;
                                case "AI":
                                    Console.WriteLine("AI mode");
                                    PATH_DATA_MAIN_TRAIN = PATH_DATA_TRAIN_AI;
                                    break;
                                default:
                                    PATH_DATA_MAIN_TRAIN = PATH_DATA_TRAIN_REAL;
                                    break;
                            }
                                int numb_Layer = int.Parse(record[1]);
                                var NEW_HIDDEN_NODE = new int[numb_Layer];
                                for (i = 0; i < numb_Layer; i++)
                                {
                                    NEW_HIDDEN_NODE[i] = int.Parse(record[2 + i]);
                                }
                                HIDDEN_NODE[dem - 3] = NEW_HIDDEN_NODE;
                                var NewTimeSnap = new List<int>();
                                    for (i = 2 + numb_Layer; i < record.Length; i++)
                                    {
                                        NewTimeSnap.Add(int.Parse(record[i]));
                                    }
                                    timeSnap.Add(NewTimeSnap);
                            
                        }
                        else
                        {
                            Console.WriteLine("Get record fail!");
                            continue;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Get File config fail!");
                }
                configContent.Dispose();
            }
            string contentRecord = string.Empty;
            for (i = 0; i < HIDDEN_NODE.Length; i++)
            {
                contentRecord += HIDDEN_NODE[i].Length + " ";
                for (int j = 0; j < HIDDEN_NODE[i].Length; j++)
                {
                    contentRecord += HIDDEN_NODE[i][j] + " ";
                }
                foreach (var item in timeSnap[i])
                {
                    contentRecord += item + " ";
                }
                contentRecord += "\n";
            }
            
            #endregion
            #region time pos
            using (var time_pos = File.OpenText(TIME_POS))
            {
                if (time_pos != null)
                {
                    string line = string.Empty;
                    int dem = 0;
                    string[] record;
                    while ((line = time_pos.ReadLine()) != null)
                    {
                        dem++;
                        if (dem == 1)
                        {
                            var timePos_content = line.Split(' ');
                            if(timePos_content.Length != 2)
                            {
                                Console.WriteLine("Time pos format wrong!");
                                break;
                            }
                            s_current_Time[0] = int.Parse(timePos_content[0]);
                            s_current_Time[1] = int.Parse(timePos_content[1]);
                        }
                        if(dem == 2)
                        {
                            NAME_RESUME_NETWORK = line;
                        }
                    }
                }
                contentRecord += s_current_Time[0] + " " + s_current_Time[1]+ " " + (NAME_RESUME_NETWORK == string.Empty?"New network ": NAME_RESUME_NETWORK) + "\n";
                Console.WriteLine(contentRecord);
                time_pos.Dispose();
            }
                        #endregion
        return true;
        }
    }
}
